package com.stackfing.handgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandgoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandgoApplication.class, args);
	}
}
